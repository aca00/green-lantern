package com.aca.greenlantern.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StockSymbolDto {
    private String key;
    private String symbol;
}

package com.aca.greenlantern.entity;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GLStockListEntity {
    Integer id;
    String listName;
    String key;
    Integer hidden;
}

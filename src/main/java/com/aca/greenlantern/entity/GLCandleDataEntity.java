package com.aca.greenlantern.entity;

import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GLCandleDataEntity {
    Integer id;
    String key;
    Long candleTime;
    Integer candleOpen;
    Integer candleHigh;
    Integer candleLow;
    Integer candleClose;
    Integer candleVol;
    Integer candleOpenInterest;
}

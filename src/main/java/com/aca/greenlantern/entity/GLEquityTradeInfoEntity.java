package com.aca.greenlantern.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GLEquityTradeInfoEntity {
    Integer id;
    String key;
    Long fetchedOn;
    Integer bid1Price;
    Integer bid1Qty;
    Integer bid2Price;
    Integer bid2Qty;
    Integer bid3Price;
    Integer bid3Qty;
    Integer bid4Price;
    Integer bid4Qty;
    Integer bid5Price;
    Integer bid5Qty;
    Integer ask1Price;
    Integer ask1Qty;
    Integer ask2Price;
    Integer ask2Qty;
    Integer ask3Price;
    Integer ask3Qty;
    Integer ask4Price;
    Integer ask4Qty;
    Integer ask5Price;
    Integer ask5Qty;
    Integer totalBuyQuantity;
    Integer totalSellQuantity;
    Integer totalTradedVolume;
    Integer quantityTraded;
    Integer deliveryQuantity;
    Integer deliveryToTradedQuantity;
    Integer impactCost;
    String cmDailyVolatility;
    String cmAnnualVolatility;
    Integer securityVar;
    Integer indexVar;
    Integer varMargin;
    Integer extremeLossMargin;
    Integer adhocMargin;
    Integer applicableMargin;

    @SuppressWarnings("unchecked")
    @JsonProperty("marketDeptOrderBook")
    private void unpackMarketDeptOrderBook(Map<String, Object> book) {
        if (book == null)
            return;
        List<Map<String, Number>> bids =
                (List<Map<String, Number>>) book.computeIfAbsent("bid", (x) -> new ArrayList<>());
        List<Map<String, Number>> asks =
                (List<Map<String, Number>>) book.computeIfAbsent("ask", (x) -> new ArrayList<>());

        bids.forEach(map -> map.put("price", handleDouble(map.get("price"))));
        asks.forEach(map -> map.put("price", handleDouble(map.get("price"))));

        if (!bids.isEmpty()) {
            bid1Price = (Integer) bids.getFirst().get("price");
            bid1Qty = (Integer) bids.getFirst().get("quantity");
        }

        if (bids.size() > 1) {
            bid2Price = (Integer) bids.get(1).get("price");
            bid2Qty = (Integer) bids.get(1).get("quantity");
        }
        if (bids.size() > 2) {
            bid3Price = (Integer) bids.get(2).get("price");
            bid3Qty = (Integer) bids.get(2).get("quantity");
        }
        if (bids.size() > 3) {
            bid4Price = (Integer) bids.get(3).get("price");
            bid4Qty = (Integer) bids.get(3).get("quantity");
        }
        if (bids.size() > 4) {
            bid5Price = (Integer) bids.get(4).get("price");
            bid5Qty = (Integer) bids.get(4).get("quantity");
        }

        if (!asks.isEmpty()) {
            ask1Price = (Integer) asks.getFirst().get("price");
            ask1Qty = (Integer) asks.getFirst().get("quantity");
        }

        if (asks.size() > 1) {
            ask2Price = (Integer) asks.get(1).get("price");
            ask2Qty = (Integer) asks.get(1).get("quantity");
        }
        if (asks.size() > 2) {
            ask3Price = (Integer) asks.get(2).get("price");
            ask3Qty = (Integer) asks.get(2).get("quantity");
        }
        if (asks.size() > 3) {
            ask4Price = (Integer) asks.get(3).get("price");
            ask4Qty = (Integer) asks.get(3).get("quantity");
        }
        if (asks.size() > 4) {
            ask5Price = (Integer) asks.get(4).get("price");
            ask5Qty = (Integer) asks.get(4).get("quantity");
        }
        totalBuyQuantity = (Integer) book.get("totalBuyQuantity");
        totalSellQuantity = (Integer) book.get("totalSellQuantity");

        Map<String, Number> valueAtRisk = (Map<String, Number>) book.get("valueAtRisk");
        if (valueAtRisk != null) {
            securityVar = handleDouble(valueAtRisk.get("securityVar"));
            indexVar = handleDouble(valueAtRisk.get("indexVar"));
            varMargin = handleDouble(valueAtRisk.get("varMargin"));
            extremeLossMargin = handleDouble(valueAtRisk.get("extremeLossMargin"));
            adhocMargin = handleDouble(valueAtRisk.get("adhocMargin"));
            applicableMargin = handleDouble(valueAtRisk.get("applicableMargin"));
        }

        Map<String, Object> tradeInfo = (Map<String, Object>) book.get("tradeInfo");
        if (tradeInfo != null) {
            totalTradedVolume = (Integer) tradeInfo.computeIfAbsent("totalTradedVolume", x -> -1);
            impactCost = handleDouble(tradeInfo.get("impactCost"));
            cmDailyVolatility = (String) tradeInfo.get("cmDailyVolatility");
            cmAnnualVolatility = (String) tradeInfo.get("cmAnnualVolatility");
        }
    }

    @JsonProperty("securityWiseDP")
    private void unpackSecurityWiseDP(Map<String, Object> securityWiseDP) {
        if (securityWiseDP == null)
            return;
        quantityTraded = (Integer) securityWiseDP.get("quantityTraded");
        deliveryQuantity = (Integer) securityWiseDP.get("deliveryQuantity");
        deliveryToTradedQuantity = handleDouble(securityWiseDP.get("deliveryToTradedQuantity"));

    }

    private Integer handleDouble(Object num) {
        return switch (num) {
            case Integer i -> i;
            case Double i -> (int) Math.round(i * 100);
            default -> -1;
        };
    }
}

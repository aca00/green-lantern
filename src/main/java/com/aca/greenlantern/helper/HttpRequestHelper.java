package com.aca.greenlantern.helper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Slf4j
@Component
public class HttpRequestHelper {
    private final HttpClient client;
    private CookieManager cookieManager;

    public HttpRequestHelper() {
        this.client = HttpClient.newBuilder()
                .cookieHandler(getCookieManager())
                .build();
    }

    public CookieManager getCookieManager() {
        if (this.cookieManager == null) {
            this.cookieManager = new CookieManager();
            this.cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        }
        return this.cookieManager;
    }

    public boolean clearAllCookies() {
        if (this.cookieManager == null) {
            this.cookieManager = getCookieManager();
        }
        return this.cookieManager.getCookieStore().removeAll();
    }

    public boolean clearCookieOfSpecificUrl(URI uri) {
        List<HttpCookie> cookies = this.cookieManager.getCookieStore().get(uri);
        try {
            cookies.forEach(cookie -> {
                this.cookieManager.getCookieStore().remove(uri, cookie);
            });
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }

        return true;

    }

    public boolean fetchCookies(String url) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Accept", "application/json")
                .header("User-Agent", "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36")
                .build();
        try {
            HttpResponse<String> response = this.send(request);
            if (response.statusCode() == 200) {
                return true;
            }
        } catch (Exception e) {
            throw new RuntimeException("Couldn't set cookies");
        }
        return false;
    }

    public <T> HttpResponse<T> send(HttpRequest request, HttpResponse.BodyHandler<T> bodyHandler) throws IOException, InterruptedException {
        return this.client.send(request, bodyHandler);
    }

    public HttpResponse<String> send(HttpRequest request) throws IOException, InterruptedException {
        return this.client.send(request, HttpResponse.BodyHandlers.ofString());
    }

}

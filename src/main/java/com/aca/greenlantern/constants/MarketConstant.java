package com.aca.greenlantern.constants;

import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.ZoneOffset;

public  class MarketConstant {
    public static final ZoneOffset ZONE_OFFSET_INDIA = ZoneOffset.ofHoursMinutes(5, 30);
    public static final LocalTime MARKET_OPEN_TIME = LocalTime.of(9, 15);
    public static final LocalTime MARKET_CLOSE_TIME = LocalTime.of(16, 0);

}

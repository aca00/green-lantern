package com.aca.greenlantern.config;


import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Configuration
@Slf4j
public class SqliteConfig {
    @Autowired
    private ResourceLoader resourceLoader;
    private static final String DRIVER_CLASS_NAME = "org.sqlite.JDBC";

    @Value("${config.sqlite.db-url}")
    private String dbUrl;

//    @Bean
//    public DataSource dataSource() {
////        String dbUrl = "jdbc:sqlite:/home/aca/ideaProjects/satellite/transponder/src/main/resources/static/satellite.db";
//        DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
//        dataSourceBuilder.driverClassName(DRIVER_CLASS_NAME);
//        log.debug(dbUrl);
//        dataSourceBuilder.url(dbUrl);
//        return dataSourceBuilder.build();
//    }

    @PostConstruct
    public void initializeSchema() {
        try (Connection connection = DriverManager.getConnection(dbUrl)) {
            ScriptRunner scriptRunner = new ScriptRunner(connection);
            InputStream schemaStream = resourceLoader.getResource("classpath:/sqlite/schema.sql")
                    .getInputStream();
            BufferedReader inputStreamReader = new BufferedReader(new InputStreamReader(schemaStream));
            scriptRunner.runScript(inputStreamReader);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
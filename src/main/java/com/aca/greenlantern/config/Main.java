package com.aca.greenlantern.config;


import org.json.JSONObject;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.*;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;
import java.util.stream.Stream;

class Main {

    public static void main(String[] args) {

        CookieManager cm = new CookieManager();
        cm.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

//        String url = "https://api.upstox.com/v2/historical-candle/NSE_EQ%7CINE848E01016/1minute/2023-11-13/2023-11-12";
        String url = "https://www.nseindia.com/";
        HttpClient httpClient = HttpClient.newBuilder()
                .cookieHandler(cm)
                .build();

        HttpRequest httpRequest = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .header("Accept", "application/json")
                .header("User-Agent", "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

            // Check the response status
            if (httpResponse.statusCode() == 200) {
                List<HttpCookie> cookies = Optional.ofNullable(
                                httpResponse.headers().map().get("set-cookie")
                        ).orElse(Collections.emptyList())
                        .stream().map(HttpCookie::parse)
                        .flatMap(Collection::stream).toList();

                cookies.forEach(cookie -> {
                    try {
                        cm.put(
                                URI.create("https://www.nseindia.com/api/quote-equity"),
                                httpResponse.headers().map()
                                );
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });



                String url1 = "https://www.nseindia.com/api/quote-equity?symbol=PAYTM";
                HttpRequest httpRequest1 = HttpRequest.newBuilder()
                        .uri(URI.create(url1))
                        .header("Accept", "application/json")
                        .header("User-Agent", "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36")
                        .build();


                HttpResponse<String> resp = httpClient.send(httpRequest1, HttpResponse.BodyHandlers.ofString());


                JSONObject obj = new JSONObject(resp.body());
                // Do something with the response body (e.g., print it)
                System.out.println(httpResponse.body());
            } else {
                // Print an error message if the request was not successful
                System.err.println("Error: " + httpResponse.statusCode() + " - " + httpResponse.body());
            }
        } catch (Exception e) {
            // Handle exceptions
            e.printStackTrace();
        }
    }
}

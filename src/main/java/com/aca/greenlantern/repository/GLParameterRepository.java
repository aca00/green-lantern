package com.aca.greenlantern.repository;

import com.aca.greenlantern.entity.GLParameterEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface GLParameterRepository extends CommonRepository<GLParameterEntity> {
}

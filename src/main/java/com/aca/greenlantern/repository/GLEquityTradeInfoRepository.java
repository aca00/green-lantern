package com.aca.greenlantern.repository;

import com.aca.greenlantern.entity.GLEquityTradeInfoEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GLEquityTradeInfoRepository extends CommonRepository<GLEquityTradeInfoEntity> {
}

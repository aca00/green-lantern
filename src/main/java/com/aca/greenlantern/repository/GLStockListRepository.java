package com.aca.greenlantern.repository;

import com.aca.greenlantern.dto.StockSymbolDto;
import com.aca.greenlantern.entity.GLStockListEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface GLStockListRepository extends CommonRepository<GLStockListEntity> {
    List<StockSymbolDto> findByListName(@Param("listName") String listName);
}

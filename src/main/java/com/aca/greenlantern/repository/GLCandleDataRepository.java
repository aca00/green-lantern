package com.aca.greenlantern.repository;

import com.aca.greenlantern.entity.GLCandleDataEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface GLCandleDataRepository extends CommonRepository<GLCandleDataEntity> {
}

package com.aca.greenlantern.repository;

public interface CommonRepository <T>{
    T save(T item);
    T update(T item);
    int delete(T item);
}

package com.aca.greenlantern;

import com.aca.greenlantern.entity.GLEquityTradeInfoEntity;
import com.aca.greenlantern.service.NSEWebHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Map;

@SpringBootApplication
@EnableScheduling
public class GreenLanternApplication implements CommandLineRunner {
    @Autowired
    NSEWebHelper nseWebHelper;

    @Autowired
    ObjectMapper objectMapper;

    public static void main(String[] args) {
        SpringApplication.run(GreenLanternApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        //        GLEquityTradeInfoEntity entity = objectMapper.readValue(json, GLEquityTradeInfoEntity.class);
        System.out.println("Hi");
//        nseWebHelper.fetchVolume();
        //        mapToEntity();

    }

    private GLEquityTradeInfoEntity mapToEntity() throws JsonProcessingException {
        String json =
                "{\"noBlockDeals\": true, \"bulkBlockDeals\": [{\"name\": \"Session I\"}, {\"name\": \"Session II\"}], \"marketDeptOrderBook\": {\"totalBuyQuantity\": 645102, \"totalSellQuantity\": 1763526, \"bid\": [{\"price\": 141.65, \"quantity\": 1943}, {\"price\": 141.6, \"quantity\": 12730}, {\"price\": 141.55, \"quantity\": 7046}, {\"price\": 141.5, \"quantity\": 3528}, {\"price\": 141.45, \"quantity\": 7572}], \"ask\": [{\"price\": 141.8, \"quantity\": 94}, {\"price\": 141.85, \"quantity\": 2282}, {\"price\": 141.9, \"quantity\": 2625}, {\"price\": 141.95, \"quantity\": 4168}, {\"price\": 142, \"quantity\": 15320}], \"tradeInfo\": {\"totalTradedVolume\": 106299, \"totalTradedValue\": 150.57, \"totalMarketCap\": 0, \"ffmc\": 0, \"impactCost\": 0, \"cmDailyVolatility\": null, \"cmAnnualVolatility\": null, \"marketLot\": \"\", \"activeSeries\": \"EQ\"}, \"valueAtRisk\": {\"securityVar\": 0, \"indexVar\": 0, \"varMargin\": 0, \"extremeLossMargin\": 0, \"adhocMargin\": 0, \"applicableMargin\": 0}}, \"securityWiseDP\": {\"quantityTraded\": 0, \"deliveryQuantity\": 0, \"deliveryToTradedQuantity\": 0, \"seriesRemarks\": null, \"secWiseDelPosDate\": \"-\"}}";

        Map<String, Object> map = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
        });

        return null;
    }
}

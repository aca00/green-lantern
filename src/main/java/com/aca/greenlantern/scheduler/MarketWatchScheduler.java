package com.aca.greenlantern.scheduler;

import com.aca.greenlantern.service.NSEWebHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MarketWatchScheduler {
    @Autowired
    private NSEWebHelper nseWebHelper;

    // cron job starts at 9:16 AM to make marketHours() condition true.
    @Scheduled(cron = "0 16 9 ? * MON-FRI", zone = "Asia/Kolkata")
    public void watchMarket() {
        try {
            nseWebHelper.fetchVolume();
        } catch (Exception e) {
            log.error("Couldn't start fetchVolume. Exception : " + e.getMessage());
        }

    }
}

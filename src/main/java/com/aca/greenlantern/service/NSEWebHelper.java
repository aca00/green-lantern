package com.aca.greenlantern.service;

import com.aca.greenlantern.constants.MarketConstant;
import com.aca.greenlantern.dto.StockSymbolDto;
import com.aca.greenlantern.entity.GLEquityTradeInfoEntity;
import com.aca.greenlantern.helper.HttpRequestHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Service
@NoArgsConstructor
@Slf4j
public class NSEWebHelper {
    @Autowired
    private HttpRequestHelper requestHelper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private GLEquityTradeInfoService glEquityTradeInfoService;

    @Autowired
    private GLStockListService glStockListService;

    @Setter
    private List<StockSymbolDto> stocks;
    private static final String NSE_QUOTE_EQUITY_TRADE_INFO_URL_TEMPLATE = "https://www.nseindia.com/api/quote-equity?symbol=%s&section=trade_info";
    private static final String NSE_COOKIE_FETCH_URL = "https://www.nseindia.com/";

    private record RequestExecutorResponse(boolean isSuccess, String message) {
    }

    @AllArgsConstructor
    private static class RequestExecutor implements Callable<RequestExecutorResponse> {
        private HttpRequestHelper httpRequestHelper;
        private HttpRequest request;
        private ObjectMapper objectMapper;
        private GLEquityTradeInfoService glEquityTradeInfoService;
        private String stockKey;

        @Override
        public RequestExecutorResponse call() throws Exception {
            String message;
            try {
                HttpResponse<String> httpResponse = httpRequestHelper.send(request);
                if (httpResponse.statusCode() != 200) {
                    message = "Request unsuccessful. Status code : " + httpResponse.statusCode();
                    return new RequestExecutorResponse(false, message);
                }

                String jsonResponse = httpResponse.body();
                GLEquityTradeInfoEntity tradeInfoEntity = objectMapper.readValue(jsonResponse, GLEquityTradeInfoEntity.class);
                tradeInfoEntity.setFetchedOn(System.currentTimeMillis());
                tradeInfoEntity.setKey(stockKey);
                glEquityTradeInfoService.save(tradeInfoEntity);

            } catch (Exception e) {
                log.error("Exception " + e.getMessage());
                return new RequestExecutorResponse(false, "Exception occurred: " + e.getMessage());
            }

            return new RequestExecutorResponse(true, "Success");

        }
    }


    public void fetchVolume() throws InterruptedException {
        this.stocks = glStockListService.findStockListByName("list1");
        requestHelper.fetchCookies(NSE_COOKIE_FETCH_URL);
        if (stocks == null) {
            throw new RuntimeException("stock keys can't be null");
        }

        int wave = 0;

        while (marketHours()) {
            log.info("NSEWebservice wave : " + ++wave);
            long waveStartTime = System.currentTimeMillis();

            List<Future<RequestExecutorResponse>> futures = new ArrayList<>(stocks.size());

            for (StockSymbolDto stock : stocks) {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(String.format(NSE_QUOTE_EQUITY_TRADE_INFO_URL_TEMPLATE, stock.getSymbol())))
                        .header("Accept", "application/json")
                        .header("User-Agent", "Mozilla/5.0 (X11; CrOS x86_64 12871.102.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.141 Safari/537.36")
                        .build();

                try (ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor()) {
                    Future<RequestExecutorResponse> future = executor.submit(
                            new RequestExecutor(requestHelper, request, objectMapper, glEquityTradeInfoService, stock.getKey())
                    );
                    futures.add(future);
                } catch (Exception exception) {
                    log.error(exception.getMessage());
                }

            }

            boolean anyError = futures.stream().anyMatch(future -> {
                try {
                    RequestExecutorResponse response = future.get();
                    return response.isSuccess();
                } catch (Exception e) {
                    log.error(e.getMessage());
                    return true;
                }
            });

            if (anyError) {
                log.info("Fetching cookie at wave " + wave);
                requestHelper.fetchCookies(NSE_COOKIE_FETCH_URL);
            }

            long waveEndTime = System.currentTimeMillis();
            long duration = waveEndTime - waveStartTime;
            log.info("Wave " + wave + " ended. Took " + duration + " milli seconds.");

            long sleepTime = duration < 60000 ? 60000 - duration : 0;

            Thread.sleep(sleepTime);

        }
        log.info("Market closed.");
    }

    private boolean marketHours() {
        return LocalTime.now(MarketConstant.ZONE_OFFSET_INDIA).isAfter(MarketConstant.MARKET_OPEN_TIME) &&
                LocalTime.now(MarketConstant.ZONE_OFFSET_INDIA).isBefore(MarketConstant.MARKET_CLOSE_TIME);
    }

    public static void main(String[] args) {
        NSEWebHelper nseWebHelper = new NSEWebHelper();
        System.out.println(nseWebHelper.marketHours());
    }

}

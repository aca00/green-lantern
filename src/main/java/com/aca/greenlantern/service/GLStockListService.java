package com.aca.greenlantern.service;

import com.aca.greenlantern.dto.StockSymbolDto;
import com.aca.greenlantern.entity.GLStockListEntity;
import com.aca.greenlantern.repository.GLStockListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GLStockListService {
    private final GLStockListRepository glStockListRepository;

    @Autowired
    public GLStockListService(GLStockListRepository glStockListRepository) {
        this.glStockListRepository = glStockListRepository;
    }

    List<StockSymbolDto> findStockListByName(String listName) {
        return glStockListRepository.findByListName(listName);
    }
}

package com.aca.greenlantern.service;

import com.aca.greenlantern.entity.GLEquityTradeInfoEntity;
import com.aca.greenlantern.repository.GLEquityTradeInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GLEquityTradeInfoService {
    private final GLEquityTradeInfoRepository glEquityTradeInfoRepository;

    @Autowired
    public GLEquityTradeInfoService(GLEquityTradeInfoRepository glEquityTradeInfoRepository) {
        this.glEquityTradeInfoRepository = glEquityTradeInfoRepository;
    }

    public GLEquityTradeInfoEntity save(GLEquityTradeInfoEntity item) {
        return glEquityTradeInfoRepository.save(item);
    }
}

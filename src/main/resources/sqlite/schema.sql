DROP TABLE IF EXISTS gl_stock_lists;
CREATE TABLE gl_stock_lists
(
  _id integer PRIMARY KEY NOT NULL,
  list_name VARCHAR(128) NOT NULL,
  _key VARCHAR(64) ,
  hidden integer,
  UNIQUE(list_name,_key)
);
DROP TABLE IF EXISTS gl_parameters;
CREATE TABLE gl_parameters
(
  _id integer PRIMARY KEY NOT NULL,
  type VARCHAR(64) ,
  _key VARCHAR(64) ,
  _value text,
  UNIQUE(type,_key,_value)
);
DROP TABLE IF EXISTS gl_equity_trade_info;
CREATE TABLE gl_equity_trade_info
(
  _id integer PRIMARY KEY NOT NULL,
  _key VARCHAR(64) ,
  fetched_on integer,
  bid1_price integer,
  bid1_qty integer,
  bid2_price integer,
  bid2_qty integer,
  bid3_price integer,
  bid3_qty integer,
  bid4_price integer,
  bid4_qty integer,
  bid5_price integer,
  bid5_qty integer,
  ask1_price integer,
  ask1_qty integer,
  ask2_price integer,
  ask2_qty integer,
  ask3_price integer,
  ask3_qty integer,
  ask4_price integer,
  ask4_qty integer,
  ask5_price integer,
  ask5_qty integer,
  total_buy_quantity integer,
  total_sell_quantity integer,
  total_traded_volume integer,
  quantity_traded integer,
  delivery_quantity integer,
  delivery_to_traded_quantity integer,
  impact_cost integer,
  cm_daily_volatility integer,
  cm_annual_volatility integer,
  security_var integer,
  index_var integer,
  var_margin integer,
  extreme_loss_margin integer,
  adhoc_margin integer,
  applicable_margin integer
);
DROP TABLE IF EXISTS gl_candle_data;
CREATE TABLE gl_candle_data
(
  _id PRIMARY KEY NOT NULL,
  _key VARCHAR(64) ,
  candle_time integer,
  candle_open integer,
  candle_high integer,
  candle_low integer,
  candle_close integer,
  candle_vol integer,
  candle_open_interest integer
);

insert into gl_parameters(type, _key, _value)
values
('nse.web.symbol', 'tatasteel', 'TATASTEEL'),
('nse.web.symbol', 'paytm', 'PAYTM'),
('nse.web.symbol', 'reliance', 'RELIANCE');

insert into gl_stock_lists(list_name,_key,hidden)
values
('list1', 'tatasteel', 0),
('list1', 'paytm', 0),
('list1', 'reliance', 0);